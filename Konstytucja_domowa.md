# Konstytucja Domowa

My, Rodzice, mając na celu pomyślność i dobro naszej Rodziny, ustalamy tę Konstytucję Domową aby jasno określić prawa panujące w naszym Domu.

## Rozdział 1. Definicje i zasady ogólne

Rodzina składa się z Rodziców i Dzieci.

Dom jest miejscem życia i dobrem wspólnym całej Rodziny.

Źródłem prawa w Domu są Rodzice, którzy decydują jednogłośnie.

Konstytucja Domowa wchodzi w życie z dniem ogłoszenia, i może ulec zmianie jedynie decyzją Rodziców.

## Rozdział 2. Prawa i obowiązki

### Prawa i obowiązki podstawowe

Każdy członek Rodziny ma prawo:

- do zapewnienia podstawowych potrzeb życiowych
- do pomocy i opieki ze strony innych członków Rodziny
- do szacunku i wysłuchania ze strony innych członków Rodziny
- do przebywania w przyjaznej i bezpiecznej atmosferze

Każdy członek Rodziny ma obowiązek:

- nauki i pracy odpowiednio do wieku i zdolności
- dbania o swoje własne zdrowie fizyczne i psychiczne
- dbania o czystość swoją, swojego pokoju i całego Domu
- komunikacji z innymi członkami Rodziny
- uczestnictwa w pracach związanych z gospodarstwem domowym

Rodzice mają obowiązek dbać o zdrowie i prawidłowy rozwój Dzieci.

Dziecko ma prawo do opieki, utrzymania i zapewnienia potrzeb rozwojowych stosownych do jego wieku. 

Obowiązkiem Dziecka jest wykonywanie poleceń Rodziców.

Podstawowe prace związane z gospodarstwem domowym dzielone są na wszystkich członków Rodziny.
Obejmuje to sprzątanie (zwłaszcza po sobie!), opiekę nad zwierzętami, zmywanie, pranie,
odkurzanie. Szczegółowy podział obowiązków określa osobne rozporządzenie (Plan obowiązków).

### Prawa i obowiązki dodatkowe

Prawa i obowiązki Dzieci są dostosowane do ich poziomu rozwoju, i harmonizowane w taki sposób, że z przybywaniem obowiązków przybywa także praw.

Zatem w miarę dorastania, Dzieci dostają kolejne obowiązki. Przykładowo:

- obowiązek większego udziału w pracach domowych, 
- obowiązek samodzielnego załatwiania swoich spraw,
- obowiązek sprawowania opieki nad innymi członkami Rodziny,
- obowiązek wytężonej pracy w szkole.

Wraz z obowiązkami rosną także prawa Dzieci. Przykładowo, Dziecko może zyskać takie prawa dodatkowe jak:

- prawo do kieszonkowego,
- prawo nielimitowanego dostepu do internetu i mediów,
- prawo dodatkowych atrakcji jak imprezy towarzyskie i kulturalne,
- prawo zapraszania znajomych i przyjaciół do domu,
- prawo pobierania dodatkowej - płatnej edukacji,
- prawo samodzielnego decydowania o swojej przyszłości.

Szczegółowy zakres praw i obowiązków dodatkowych ustalają Rodzice w miarę dorastania Dzieci.

## Rozdział 3. Wymiar sprawiedliwości

### Konsekwencje naruszenia prawa

W stosunku do Dziecka, które łamie prawo domowe, Rodzic może zastosować konsekwencje w postaci ograniczenia praw dodatkowych. 
Dokładny zakres ograniczenia zależy od stopnia naruszenia prawa i decyzji Rodzica.
Przykładowo, niespełnianie obowiązków przez jeden dzień może skutkować odcięciem internetu na dzień kolejny.

W stosunku do Rodzica, który łamie prawo domowe, konsekwencje może wyciągać jedynie drugi Rodzic,
lub instytucje nadrzędne w stosunku do Domu (np. władze królestwa czy republiki, na terenie której Dom się znajduje).

### Rozwiązywanie sporów i proces stanowienia prawa

Jeżeli dowolny członek Rodziny uważa, że naruszono przepisy niniejszej Konstytucji, 
ma prawo to zgłosić i zażądać narady całego Domu celem rozwiązania problemu. 
Rodzice zwołują naradę w najbliższym możliwym czasie.  
Rozstrzygnięcia zapadłe w czasie narady są wiążące i stają się częścią prawa domowego. 
Na naradzie każdy musi zostać wysłuchany, jednak głos ostateczny mają Rodzice. 
Wybrane przez Rodziców kwestie mogą zostać poddane głosowaniu, w którym głos Rodzica ma większą
wagę niż głos Dziecka, np Dziecko ma 1 głos, a Rodzic 1,5 głosu.
