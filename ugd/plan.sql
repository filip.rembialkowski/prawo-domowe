\set ON_ERROR_STOP 1

SET lc_time TO 'pl_PL.UTF-8';

BEGIN;

CREATE TABLE ugd_zadania (
    id int PRIMARY KEY,
    nazwa text NOT NULL UNIQUE,
    ikonka text CHECK (ikonka <> ''),
    okres int NOT NULL CHECK (okres > 0),
    waga int NOT NULL CHECK (waga >= 0),
    pomijaj_dni text[] NOT NULL DEFAULT '{}'::int[],
    opis text
);

COMMENT ON TABLE ugd_zadania IS 'Definicje zadań UGD';
COMMENT ON COLUMN ugd_zadania.okres IS 'Co ile dni trzeba to robić?';
COMMENT ON COLUMN ugd_zadania.waga IS 'Ile minut mniej więcej to zajmuje?';

COPY ugd_zadania (id,nazwa,ikonka,okres,waga,pomijaj_dni,opis) FROM STDIN;
1	Odkurzanie	🧹	7	120	{}	W całym mieszkaniu: pozbieraj mniejsze rzeczy z podłogi, podnieś dywaniki itp. Odkurz podłogę. Jeśli coś jest rozlane lub poplamione, przetrzyj to.
2	Łazienka	🚾	7	120	{}	Pozbieraj rzeczy, odkurz całą łazienkę, umyj WC, umywalkę, kabinę prysznicową, podłogę, przetrzyj lustro.
3	Pranie	🧦	1	45	{Wtorek,Niedziela}	Zbierz (suche) ubrania z suszarki i rozdaj je ludziom. Puść pranie (o ile jest co prać) i je powieś.
4	Zmywanie	🍴	1	45	{}	Napełnij zmywarkę, puść zmywanie i włóż czyste naczynia do szafek. Zadbaj o to aby pod koniec dnia w kuchni nie było brudnych naczyń.
5	Karmienie zwierząt	🍖	1	15	{}	Zadbaj, by zwierzęta miały czystą wodę i karmę.
6	Śmieci	🗑	1	15	{}	Sprawdź wszystkie wspólne kosze (kuchnia, łazienka) i jeżeli któryś jest pełny lub śmierdzi to wynieś śmieci i włóż nową torbę.
7	Kąpanie psa	🐕🚿	28	90	{}	Wykąp psa i posprzątaj brodzik.
8	Kuweta	🐈	7	20	{}	Posprzątaj kocią kuwetę.
\.

CREATE TABLE ugd_osoby (
    id integer PRIMARY KEY,
    nazwa text NOT NULL UNIQUE,
    udzwig integer NOT NULL
);
COMMENT ON COLUMN ugd_osoby.udzwig IS 'Limit pracy / dobę w minutach';

COPY ugd_osoby (id, nazwa, udzwig) FROM STDIN;
1	MAMA	200
2	TATA	200
3	ALA	200
4	MAKS	200
\.

CREATE TABLE ugd_kalendarz (
    data date PRIMARY KEY,
    data_abs integer GENERATED ALWAYS AS (extract(julian from data)) STORED,
    rok integer GENERATED ALWAYS AS (extract(year from data)) STORED,
    miesiac integer GENERATED ALWAYS AS (extract(month from data)) STORED,
    dzien integer GENERATED ALWAYS AS (extract(day from data)) STORED,
    tydzien integer GENERATED ALWAYS AS (extract(week from data)) STORED,
    tydzien_abs integer GENERATED ALWAYS AS (extract(julian from data) / 7) STORED,
    dzien_tyg integer GENERATED ALWAYS AS (extract(isodow from data)) STORED,
    dzien_tyg_nazwa text GENERATED ALWAYS AS (CASE extract(isodow from data) WHEN 1 THEN 'Poniedziałek'
    WHEN 2 THEN 'Wtorek' WHEN 3 THEN 'Środa' WHEN 4 THEN 'Czwartek' WHEN 5 THEN 'Piątek' WHEN 6
        THEN 'Sobota' WHEN 7 THEN 'Niedziela' END) STORED
);

INSERT INTO ugd_kalendarz (data)
SELECT g FROM generate_series(
    '2021-12-01'::date,
    '2022-12-31'::date,
    '1 day'
) AS g;

CREATE TABLE ugd_nieobecnosci (
    data date NOT NULL,
    id_osoby integer REFERENCES ugd_osoby (id),
    PRIMARY KEY (data, id_osoby)
);

-- Marylka studia
COPY ugd_nieobecnosci (data, id_osoby) FROM STDIN;
2021-11-27	1
2021-11-28	1
2021-12-11	1
2021-12-12	1
2021-12-18	1
2021-12-19	1
2022-01-08	1
2022-01-09	1
2022-01-15	1
2022-01-16	1
2022-02-05	1
2022-02-06	1
2022-02-12	1
2022-02-13	1
2022-02-26	1
2022-02-27	1
2022-03-05	1
2022-03-06	1
2022-03-19	1
2022-03-20	1
2022-03-26	1
2022-03-27	1
2022-04-09	1
2022-04-10	1
2022-04-23	1
2022-04-24	1
2022-05-07	1
2022-05-08	1
2022-05-14	1
2022-05-15	1
2022-05-28	1
2022-05-29	1
2022-06-11	1
2022-06-12	1
\.

-- Alicja 5-23 lipca
INSERT INTO ugd_nieobecnosci (data, id_osoby)
    SELECT d, 3
    FROM generate_series('2022-07-05'::date, '2022-07-23', '1 day') d;
-- Maks 15-23 lipca
INSERT INTO ugd_nieobecnosci (data, id_osoby)
    SELECT d, 4
    FROM generate_series('2022-07-15'::date, '2022-07-23', '1 day') d;

/* Ustalenia dodatkowe

PIES SPACERY 7 dni razy 3 spacery = około 20 spacerów tyogodniowo co daje 5 na osobę

ALA - woli wieczór, weekendy rano
MAMA - wtorek lub czw rano, weekendy
MAKS - wtorek środek dnia, poniedziałek srodek, sobota po poł.
FILIP - w sumie dowolnie

Osobna tabela ->

Sinus – spacery	MAMA	TATA	ALA	MAKS
Poniedziałek		🦮₁	🦮₃	🦮₂
Wtorek	🦮₁ 	🦮₃		🦮₂
Środa	🦮₃	🦮₁		🦮₂
Czwartek	🦮₁	🦮₂	🦮₃
Piątek	🦮₃	🦮₁	🦮₂
Sobota			🦮₁	🦮₂🦮₃
Niedziela	🦮₂	🦮₃	🦮₁
*/


CREATE TABLE ugd_harmonogram (
    data date REFERENCES ugd_kalendarz (data),
    id_zadania integer REFERENCES ugd_zadania (id),
    id_osoby integer REFERENCES ugd_osoby (id),
    PRIMARY KEY (data, id_zadania, id_osoby)
);

CREATE OR REPLACE FUNCTION ugd_kolejka_zero() RETURNS ugd_osoby[]
LANGUAGE plpgsql AS $$
/* Array indeksowany od zera! */
DECLARE idx int := 0; o ugd_osoby; kolejka ugd_osoby[];
BEGIN
    FOR o IN
        SELECT * FROM ugd_osoby ORDER BY id
    LOOP
        kolejka[idx] := o;
        idx := idx + 1;
    END LOOP;
    RETURN kolejka;
END
$$;

CREATE OR REPLACE FUNCTION ugd_kolejka_get(k ugd_osoby[], n int) RETURNS ugd_osoby
LANGUAGE plpgsql AS $$
BEGIN
    RETURN k[n % array_length(k,1)];
END
$$;


CREATE OR REPLACE FUNCTION ugd_kolejka_zamien(a ugd_osoby[], i int, j int)
RETURNS ugd_osoby[]
LANGUAGE plpgsql AS $$
DECLARE buf ugd_osoby;
BEGIN
    IF i <> j THEN
        RAISE NOTICE 'Zamieniam % z %', a[i % array_length(a,1)].nazwa, a[j % array_length(a,1)].nazwa;
        buf := a[i % array_length(a,1)];
        a[i % array_length(a,1)] := a[j % array_length(a,1)];
        a[j % array_length(a,1)] := buf;
    END IF;
    RETURN a;
END
$$;

CREATE OR REPLACE FUNCTION
przydziel_zadanie(z ugd_zadania, k ugd_kalendarz, o ugd_osoby)
RETURNS boolean
LANGUAGE plpgsql AS $$
/* Zwróć fałsz, jeżeli osoba jest nieobecna lub przepracowana.
W pozostałych przypadkach, dodaj wpis do harmonogramu. */
DECLARE
BEGIN
    IF EXISTS (SELECT FROM ugd_nieobecnosci WHERE data = k.data AND id_osoby = o.id) THEN
        RAISE NOTICE 'Nieobecność % w dniu % uniemożliwia %', o.nazwa, k.data, z.nazwa;
        RETURN false;
    ELSIF z.waga + (SELECT sum(z1.waga) FROM ugd_harmonogram h JOIN ugd_zadania z1 ON z1.id =
            h.id_zadania WHERE h.data = k.data AND h.id_osoby = o.id) > o.udzwig THEN
        RAISE NOTICE 'Za dużo pracy dla % w dniu %, by wykonać %', o.nazwa, k.data, z.nazwa;
        RETURN false;
    ELSIF k.dzien_tyg_nazwa = ANY (z.pomijaj_dni) THEN
        RAISE EXCEPTION 'Zadanie % nie może być planowane na %', z.nazwa, k.dzien_tyg_nazwa;
    ELSE
        INSERT INTO ugd_harmonogram (data, id_zadania, id_osoby) SELECT k.data, z.id, o.id;
        RAISE NOTICE 'Przydzielono % w dniu % (%) do: %', z.nazwa, k.data, k.dzien_tyg_nazwa, o.nazwa;
        RETURN true;
    END IF;
END
$$;

CREATE OR REPLACE PROCEDURE zaplanuj_zadania()
LANGUAGE plpgsql AS $$
DECLARE
    z ugd_zadania;
    kolejka ugd_osoby[];
    idx_osob int := 0;
    k ugd_kalendarz;
    _offset int := 0;
BEGIN
    FOR z IN
        SELECT * FROM ugd_zadania ORDER BY id
    LOOP
        RAISE NOTICE 'Planowanie zadania % (%) okres %', z.id, z.nazwa, z.okres;
        kolejka := ugd_kolejka_zero();
        idx_osob := 1 + z.id % array_length(kolejka, 1);  -- aby nie startowac w tym samym dniu
        FOR k IN
            SELECT * FROM ugd_kalendarz
            WHERE dzien_tyg % z.okres = z.okres - 1
            AND NOT dzien_tyg_nazwa = ANY(z.pomijaj_dni)
            ORDER BY data
        LOOP
            RAISE NOTICE 'Planowanie zadania % (%) na dzień % (%)', z.id, z.nazwa, k.data, k.dzien_tyg_nazwa;
            _offset := 0;
            WHILE NOT przydziel_zadanie(z, k, ugd_kolejka_get(kolejka, idx_osob)) LOOP
                kolejka := ugd_kolejka_zamien(kolejka, idx_osob, idx_osob + _offset);
                _offset := _offset + 1;
            END LOOP;
            idx_osob := idx_osob + 1;
        END LOOP;
    END LOOP;
END;
$$;

CALL zaplanuj_zadania();

\set wydruk_od '2022-12-01'
\set wydruk_do '2022-12-31'

\o plan.html
\qecho '<!DOCTYPE html>'
\qecho '<html lang="pl">'
\qecho '<head> <title>Kalendarz zadań domowych</title>'
\qecho '  <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8" />'
\qecho '  <link rel="stylesheet" href="plan.css">'
\qecho '<!-- Generated on ' `date` ' -->'
\qecho '</head><body>'
\qecho '<table class="harmonogram">'
\qecho '  <caption>Podział obowiązków</caption>'
\qecho '  <tr> <th>Data</th> <th>MAMA</th> <th>TATA</th> <th>ALA</th> <th>MAKS</th> </tr>'

/* Przykładowy HTML:
<table border="0">
  <caption>PODZIAŁ OBOWIĄZKÓW</caption>
  <tr>
    <th align="center">Data</th>
    <th align="center">MAMA</th>
    <th align="center">TATA</th>
    <th align="center">ALA</th>
    <th align="center">MAKS</th>
  </tr>
  <tr valign="top">
    <td align="left">śro 1 gru 2021</td>
    <td align="left">🍴</td>
    <td align="left">🍖</td>
    <td align="left">🗑</td>
    <td align="left">🧦</td>
  </tr>
</table>
*/

\pset format unaligned
\pset tuples_only on

WITH q AS(
-- pivot na osoby
    SELECT
        k.data,
        array_to_string(array_agg(distinct z.ikonka) FILTER (WHERE o.nazwa='MAMA'), ' ') AS "MAMA",
        array_to_string(array_agg(distinct z.ikonka) FILTER (WHERE o.nazwa='TATA'), ' ') AS "TATA",
        array_to_string(array_agg(distinct z.ikonka) FILTER (WHERE o.nazwa='ALA'), ' ') AS "ALA",
        array_to_string(array_agg(distinct z.ikonka) FILTER (WHERE o.nazwa='MAKS'), ' ') AS "MAKS"
    FROM ugd_harmonogram h
    JOIN ugd_kalendarz k ON k.data = h.data
    JOIN ugd_osoby AS o ON o.id = h.id_osoby
    JOIN ugd_zadania AS z ON z.id = h.id_zadania
    WHERE h.data BETWEEN :'wydruk_od' AND :'wydruk_do'
    GROUP BY 1
)
SELECT
    -- to_char(data, 'TMdy FMDD TMmon YYYY') AS "Data", "MAMA", "TATA", "ALA", "MAKS"
    format(
        '<tr><td class="data">%s</td><td class="iko">%s</td><td class="iko">%s</td><td class="iko">%s</td><td class="iko">%s</td></tr>',
        to_char(data, 'TMdy FMDD TMmon YYYY'), "MAMA", "TATA", "ALA", "MAKS"

    )
FROM q ORDER BY data;

\qecho '</table></body></html>'

\o

\o legenda.html
\qecho '<!DOCTYPE html>'
\qecho '<html lang="pl">'
\qecho '<head> <title>Lista zadań domowych</title>'
\qecho '  <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8" />'
\qecho '  <link rel="stylesheet" href="plan.css">'
\qecho '<!-- Generated on ' `date` ' -->'
\qecho '</head><body>'
\qecho '<table class="legenda">'
\qecho '  <caption>LEGENDA</caption>'
\qecho '  <tr> <th>Zadanie</th> <th>Symbol</th> <th>Jak często</th> <th>Opis</th> <th>± minut</th> </tr>'

SELECT
    format(
        '<tr> <td>%s</td> <td class="iko">%s</td> <td>%s</td> <td class="opis">%s</td> <td>%s</td> </tr>',
        nazwa, ikonka, CASE okres
            WHEN 1 THEN 'Codziennie' || CASE WHEN pomijaj_dni <> '{}' THEN ' (oprócz: ' || array_to_string(pomijaj_dni, ',') || ')' ELSE '' END
            WHEN 7 THEN 'Co tydzień' WHEN 28 THEN 'Co 4 tygodnie' ELSE '?' END, opis, waga
    )
FROM ugd_zadania
ORDER BY id;
\o

ROLLBACK;
